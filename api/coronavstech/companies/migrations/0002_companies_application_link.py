# Generated by Django 3.2.3 on 2021-11-02 18:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("companies", "0001_initial"),
    ]

    operations = [
        migrations.AddField(
            model_name="companies",
            name="application_link",
            field=models.URLField(blank=True),
        ),
    ]
