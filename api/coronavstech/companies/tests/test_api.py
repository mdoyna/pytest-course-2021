import json
from unittest import TestCase

import pytest
from django.test import Client
from django.urls import reverse
from api.coronavstech.companies.models import Company


@pytest.mark.django_db  # This fixture here, creates replica of our original db only for tests.
class BasicCompanyApiTestCase(TestCase):
    def setUp(self) -> None:  # setUp is a key word to use the class
        self.client = Client()  # Client will be like our "Postman"
        self.companies_url = reverse(
            "companies-list"
        )  # Instead of a link to the url http://127.0.0.1:8000/companies/ where we send
        # our request to, we can add a list. Because it might vary. This "companies-list" is automatically
        # generated. Check Django page here https://www.django-rest-framework.org/api-guide/routers/#routers

    def tearDown(self) -> None:
        pass


class TestGetCompanies(BasicCompanyApiTestCase):
    def test_zero_companies_should_return_empty_list(self) -> None:
        response = self.client.get(self.companies_url)
        self.assertEqual(response.status_code, 200)
        # We return response in a specific format, in this case JSON
        self.assertEqual(json.loads(response.content), [])

    def test_one_company_exists_should_succeed(self) -> None:
        test_company = Company.objects.create(name="Burger Company")
        response = self.client.get(self.companies_url)
        response_content = json.loads(response.content)[0]
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response_content.get("name"), test_company.name)
        self.assertEqual(response_content.get("status"), "Hiring")
        self.assertEqual(response_content.get("application_link"), "")
        self.assertEqual(response_content.get("notes"), "")

        test_company.delete()


def raise_covid19_exception() -> None:
    raise ValueError("CoronaVirus Exception!")


class TestPostCompanies(BasicCompanyApiTestCase):
    def test_create_company_without_arguments_should_fail(self) -> None:
        response = self.client.post(path=self.companies_url)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            json.loads(response.content), {"name": ["This field is required."]}
        )

    def test_create_existing_company_should_fail(self) -> None:
        Company.objects.create(name="Apple")
        response = self.client.post(path=self.companies_url, data={"name": "Apple"})
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            json.loads(response.content),
            {"name": ["company with this name already exists."]},
        )

    def test_create_company_only_with_name_all_fields_should_be_default(self) -> None:
        response = self.client.post(
            path=self.companies_url, data={"name": "Company Test"}
        )
        self.assertEqual(response.status_code, 201)
        response_content = json.loads(response.content)
        self.assertEqual(response_content.get("name"), "Company Test")
        self.assertEqual(response_content.get("status"), "Hiring")
        self.assertEqual(response_content.get("application_link"), "")
        self.assertEqual(response_content.get("notes"), "")

    def test_create_company_with_layoffs_should_succeed(self) -> None:
        response = self.client.post(
            path=self.companies_url, data={"name": "Company Test", "status": "Layoffs"}
        )
        self.assertEqual(response.status_code, 201)
        response_content = json.loads(response.content)
        self.assertEqual(response_content.get("status"), "Layoffs")

    def test_create_company_with_wrong_status_should_fail(self) -> None:
        response = self.client.post(
            path=self.companies_url,
            data={"name": "Company Test", "status": "WRONG Status"},
        )
        self.assertEqual(response.status_code, 400)
        self.assertIn("WRONG Status", str(response.content))
        self.assertIn("is not a valid choice.", str(response.content))

    @pytest.mark.xfail  # it is ok, if this test fail, it won't fail our run
    def test_should_be_ok_if_fails(self) -> None:
        self.assertEqual(1, 2)

    @pytest.mark.skip
    def test_should_be_ok_if_fails(self) -> None:
        self.assertEqual(1, 2)

    def test_raise_covid19_exception_should_pass(self) -> None:
        with pytest.raises(ValueError) as e:
            raise_covid19_exception()  # with the function that we call here, we assert that pytest will raise error
        assert "CoronaVirus Exception!" == str(
            e.value
        )  # str will unpack the value and output it. Without str we will expect to receive the type
        # of the error, which is ValueError
