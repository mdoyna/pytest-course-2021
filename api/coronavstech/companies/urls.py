from rest_framework import routers
from api.coronavstech.companies.views import CompanyViewSet

# Will create http://127.0.0.1:8000/companies/

companies_router = routers.DefaultRouter()
companies_router.register("companies", viewset=CompanyViewSet, basename="companies")
