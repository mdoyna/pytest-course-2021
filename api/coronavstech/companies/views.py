from rest_framework.viewsets import ModelViewSet
from rest_framework.pagination import PageNumberPagination
from api.coronavstech.companies.serializers import CompanySerializer
from api.coronavstech.companies.models import Company


# Create your views here.
# This ModelViewSet class will create for us everything needed Post and Get functions;
# All the standart REST API methods that we need.


class CompanyViewSet(ModelViewSet):
    serializer_class = CompanySerializer
    queryset = Company.objects.all().order_by("-last_update")
    pagination_class = PageNumberPagination
